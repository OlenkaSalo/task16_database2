select trip_no, name, plane, town_from, town_to, 
case trip_no
when 1100 then abs(timestampdiff(hour, '1900-01-01 14:30:00', '1900-01-01 17:50:00'))
when 1101 then abs(timestampdiff(hour, '1900-01-01 08:12:00', '1900-01-01 11:45:00'))
when 1181 then abs(timestampdiff(hour, '1900-01-01 12:35:00', '1900-01-01 14:30:00'))
when 1123 then abs(timestampdiff(hour, '1900-01-01 16:20:00', '1900-01-01 03:40:00'))
when 1124 then abs(timestampdiff(hour, '1900-01-01 09:00:00', '1900-01-01 19:50:00'))
when 1146 then abs(timestampdiff(hour, '1900-01-01 17:55:00', '1900-01-01 20:01:00'))
when 1187 then abs(timestampdiff(hour, '1900-01-01 15:42:00', '1900-01-01 17:39:00'))
else 1
end time_flight
from trip T, company C
where T.ID_comp = C.ID_comp;